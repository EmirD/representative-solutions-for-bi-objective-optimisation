This repository contains the code and data used in the AAAI'20 paper "Representative Solutions for Bi-Objective Optimisation" by Emir Demirović and Nicolas Schwind.

Details instructions on code usage should follow by the end of the year, but for the most part, the code should be readable as-is.

The code is written in Python 3 and makes use of the Python interface for Gurobi.

The input is a MIP file in the .mps format. It allows pure linear as well as mixed-integer programs. The code can be used to either produce the complete Pareto front (using the \epsilon-technique or a generic two-phase method), the k-representative set (see the paper), or an approximation of the k-representative set (unpublished). The input file must contain two variables named "earliness" and "tardiness", which will be used as the two objective functions in the bi-objective problem. MiniZinc users can easily convert their .mzn files into .mps by invoking Minizinc with the additional parameters "--writeModel new_file_name.mps". 

If you have any questions about the paper, the code, or the data, please do not hestitate the contact the authors:

Emir Demirović, emir.demirovic@unimelb.edu.au, https://emirdemirovic.com/

Nicolas Schwind, nicolas-schwind@aist.go.jp, https://staff.aist.go.jp/nicolas-schwind/

Paper: "Representative Solutions for Bi-Objective Optimisation"; Demirović and Schwind; to appear in AAAI’20: http://emirdemirovic.com/wp-content/uploads/2019/11/Representative-Solutions-for-Bi-Objective-Optimisation.pdf